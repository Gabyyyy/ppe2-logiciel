#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------

DROP DATABASE if exists sondageDatabase;
CREATE DATABASE sondageDatabase;
USE sondageDatabase;

#------------------------------------------------------------
# Table: sondeur
#------------------------------------------------------------

CREATE TABLE sondeur(
        idSondeur Int (3) Auto_increment  NOT NULL ,
        nom       Varchar (50) NOT NULL ,
        email     Varchar (100) NOT NULL ,
        tel       Varchar (20) NOT NULL ,
        adresse   Varchar (100) NOT NULL ,
        mdp       Varchar (255) NOT NULL,
        role      enum("user","admin")
        ,CONSTRAINT sondeur_PK PRIMARY KEY (idSondeur)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: entreprise
#------------------------------------------------------------

CREATE TABLE entreprise(
        idSondeur Int (3) NOT NULL ,
        libelle   Varchar (100) NOT NULL ,
        sigle     Varchar (100) NOT NULL ,
        siret     Varchar (100) NOT NULL,
        PRIMARY KEY (idSondeur)

        ,CONSTRAINT entreprise_sondeur_FK FOREIGN KEY (idSondeur) REFERENCES sondeur(idSondeur)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: personne
#------------------------------------------------------------

CREATE TABLE personne(
        idSondeur Int (3) NOT NULL ,
        prenom    Varchar (100) NOT NULL ,
        age       Int (3) NOT NULL,
        PRIMARY KEY (idSondeur)

        ,CONSTRAINT personne_sondeur_FK FOREIGN KEY (idSondeur) REFERENCES sondeur(idSondeur)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: categorie
#------------------------------------------------------------

CREATE TABLE categorie(
        idCategorie Int  Auto_increment  NOT NULL ,
        nom         Varchar (100) NOT NULL ,
        description Text NOT NULL
        ,CONSTRAINT categorie_PK PRIMARY KEY (idCategorie)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: sous_categorie
#------------------------------------------------------------

CREATE TABLE sous_categorie(
        idSous_categorie Int  Auto_increment  NOT NULL ,
        nom              Varchar (100) NOT NULL ,
        description      Text NOT NULL ,
        idCategorie      Int NOT NULL
        ,CONSTRAINT sous_categorie_PK PRIMARY KEY (idSous_categorie)

        ,CONSTRAINT sous_categorie_categorie_FK FOREIGN KEY (idCategorie) REFERENCES categorie(idCategorie)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: sondage
#------------------------------------------------------------

CREATE TABLE sondage(
        idSondage        Int  Auto_increment  NOT NULL,
        libelle          Varchar (100) NOT NULL,
        dateCreation     Date NOT NULL ,
        etat             enum("Ouvert", "Ferme"),
        idSondeur        Int NOT NULL ,
        idSous_categorie Int NOT NULL
        ,CONSTRAINT sondage_PK PRIMARY KEY (idSondage)

        ,CONSTRAINT sondage_sondeur_FK FOREIGN KEY (idSondeur) REFERENCES sondeur(idSondeur)
        ,CONSTRAINT sondage_sous_categorie0_FK FOREIGN KEY (idSous_categorie) REFERENCES sous_categorie(idSous_categorie)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: question
#------------------------------------------------------------

CREATE TABLE question(
        idQuestion Int  Auto_increment  NOT NULL ,
        question   Text NOT NULL ,
        idSondage  Int NOT NULL
        ,CONSTRAINT question_PK PRIMARY KEY (idQuestion)

        ,CONSTRAINT question_sondage_FK FOREIGN KEY (idSondage) REFERENCES sondage(idSondage)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: reponse
#------------------------------------------------------------

CREATE TABLE reponse(
        idReponse  Int  Auto_increment  NOT NULL ,
        reponse    Text NOT NULL ,
        nbReponse  Int NOT NULL ,
        idQuestion Int NOT NULL
        ,CONSTRAINT reponse_PK PRIMARY KEY (idReponse)

        ,CONSTRAINT reponse_question_FK FOREIGN KEY (idQuestion) REFERENCES question(idQuestion)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: media
#------------------------------------------------------------

CREATE TABLE media(
        idMedia   Int  Auto_increment  NOT NULL ,
        url       Text NOT NULL ,
        idSondage Int NOT NULL ,
        idSondeur Int NOT NULL
        ,CONSTRAINT media_PK PRIMARY KEY (idMedia)

        ,CONSTRAINT media_sondage_FK FOREIGN KEY (idSondage) REFERENCES sondage(idSondage)
        ,CONSTRAINT media_sondeur0_FK FOREIGN KEY (idSondeur) REFERENCES sondeur(idSondeur)
)ENGINE=InnoDB;




/* DONNEES */

INSERT INTO categorie (nom, description)
values 
("Politique", "Dans cette catégorie vous trouverez les sous-catégories et sondages concernant la politique française"),
("Economie", "Dans cette catégorie vous trouverez les sous-catégories et sondages concernant l'économie française"),
("Environnement", "Dans cette catégorie vous trouverez les sous-catégories et sondages concernant l'environnement en France"),
("Sport", "Dans cette catégorie vous trouverez les sous-catégories et sondages concernant le monde du sport français");


INSERT INTO sous_categorie (nom, description, idCategorie)
values 
("Presidentielles 2022", "Tous les sondages sur les élections présidentielles de 2022", 1),
("Gouvernement","Tous les sondages sur le gouvernement français", 1),
("Crypto Monnaie","Tous les sondages en rapport avec la crypto monnaie", 2),
("Pouvoir d'achat","Tous les sondages sur le pouvoir d'achat des français",2),
("Climat", "Tous les sondages sur le climat", 3),
("Pollution", "Tous les sondages sur la pollution en France", 3),
("Football", "Tous les sondages sur le monde du football français", 4),
("Rugby", "Tous les sondages sur le monde du rugby français", 4);


INSERT INTO sondeur (nom, tel, email, adresse, mdp, role)
values
("Lopes", "06123456789", "l@gmail.com", "12 rue de Paris", "abc", "admin"),
("Benachir", "07123456798", "b@gmail.com", "12 rue de Lyon", "abc", "user"),
("Axa", "0123456789", "axa@gmail.com", "20 Boulevard de Paris", "abc", "user");


INSERT INTO personne (idSondeur, prenom, age)
VALUES
(1, "Gabriel", 21),
(2, "Hamza", 19);


INSERT INTO entreprise(idSondeur, libelle, sigle, siret)
VALUES
(3, "AXA Assurance", "AXA", "123456");


INSERT INTO sondage (libelle, dateCreation, etat, idSondeur, idSous_categorie)
values
("Présidentielles",'2021-12-1',"Ouvert",1,1),
("Primaire",'2021-12-1',"Ferme",2,1),
("Pouvoir",'2021-12-2',"Ferme",1,2),
("Bitcoin",'2021-12-2',"Ouvert",2,3),
("Ethereum",'2021-12-3',"Ferme",2,3),
("Dogecoin",'2021-12-3',"Ouvert",2,3),
("PorteFeuille",'2021-12-1',"Ouvert",1,4),
("Réchauffement",'2021-12-1',"Ouvert",1,5),
("Particules",'2021-12-1',"Ouvert",2,6),
("Real Madrid",'2021-12-1',"Ouvert",1,7),
("Std Toulousain",'2021-12-3',"Ouvert",2,8);


INSERT INTO question (question, idSondage)
VALUES
("Qui selon vous gagnera les élections présidentielles de 2022 ?",1),
("Qui selon vous devrait représenter le parti des Républicain en 2022 ?",2),
("Selon vous, le président de la république dispose t-il de trop de pouvoirs ?",3),
("Le bitcoin est-il encore rentable ?",4),
("L'Ethereum sera t-il encore rentable en 2022 ?",5),
("Le Dogecoin est-il une valeur sûre ?",6),
("Les français dispose t-il d'un salaire minimum correct ?",7),
("Quelle est selon vous la meilleur résolution à prendre pour éviter le réchuaffement climatique ?",8),
("Considérez-vous votre ville comme trop polluée ?",9),
("Le Real Madrid peut-il gagner sa 14ème Ligue des champions cette année",10),
("Le std Toulousain sera t-il champion d'ici 2 ans ?",11);



INSERT INTO reponse (reponse, nbReponse, idQuestion)
VALUES
("Eric Zemmour", 1800, 1),
("Emmanuel Macron", 2500, 1),
("Jean-Luc Mélenchon", 700, 1),
("Marine Le Pen", 1900, 1),
("Yannick Jadot", 800, 1),
("Xavier Bertrand",50,2),
("Valérie Pécresse",30,2),
("Michel Barnier",25,2),
("Oui",70,3),
("Non",50,3),
("Oui",200,4),
("Non",600,4),
("Oui",800,5),
("Non",50,5),
("Oui",100,6),
("Non",100,6),
("Oui",2000, 7),
("Non",10000, 7),
("Trier",200, 8),
("Limiter l'usage d'internet",30, 8),
("Limiter l'usage de la voiture",120, 8),
("Oui",1000, 9),
("Non",20, 9),
("Oui",15, 10),
("Non",30, 10),
("Oui",20,11),
("Non",20,11);


/* VUES */

/* Une vue qui permet d'afficher les sondages en fonction de leur Sous_Categorie */
create view sondageSousCats as (
    select s.idSondage , s.libelle, s.dateCreation, s.etat, c.nom, c.idSous_categorie
    from sondage s, sous_categorie c 
    where s.idSous_categorie = c.idSous_categorie
);

/* Vue des résultats des sondages (des réponses) - gestion et vue_les_resultats.php */
CREATE VIEW vue_reponse AS SELECT r.idReponse, q.idQuestion, s.idSondage, q.question, s.dateCreation, s.etat, r.reponse, r.nbReponse, so.nom
FROM reponse r, question q, sondage s, sondeur so WHERE s.idSondage = q.idSondage AND r.idQuestion = q.idQuestion AND so.idSondeur = s.idSondeur; 

/* Vue des sondages (affichage de la question et de l'auteur (pas les réponses) - gestion et vue_les_sondages.php */
CREATE VIEW vue_sondage AS SELECT s.idSondage, s.idSous_categorie, q.idQuestion, s.libelle, q.question, so.nom, s.dateCreation, s.etat FROM question q, sondeur so, sondage s 
WHERE q.idSondage = s.idSondage AND s.idSondeur = so.idSondeur;

/* Vue des sondages et des réponses probables (pour voter) - gestion et vue_les_votes.php */
CREATE VIEW vote_sondage AS SELECT s.idSondage, so.idSondeur, q.idQuestion, r.idReponse, q.question, so.nom, s.dateCreation, s.etat, r.reponse
FROM reponse r, question q, sondeur so, sondage s WHERE q.idSondage = s.idSondage AND r.idQuestion = q.idQuestion AND so.idSondeur = s.idSondeur;


/* TRIGGERS */

/* Écrire un trigger qui permet à chaque insertion d’un sondage, l’attribut nbSondage dans sondeur sera incrémenté de un */



/* PROCEDURE */

/* Procedure qui permet d'afficher la liste des sondage selon le nom de la sous categorie passé en parametre */
delimiter $
create procedure listeSondagesListing(IN p_sous_categorie varchar(50))
begin 
      select s.nom , s.email, s.tel, s.adresse
      from sondage s, sous_categorie so
      where s.idSous_categorie = so.idCategorie
      and so.nom = p_sous_categorie;
end $
delimiter ; 

/* Procedure qui permet d'inserer des sondeurs personne*/
delimiter $
create procedure insertPersonne (IN p_nom varchar(50), IN p_email varchar(100),
IN p_tel varchar(20), IN p_adresse varchar(100), IN p_mdp varchar(255), IN p_role enum("user","admin"), IN p_prenom varchar(100), IN p_age int(3))
begin
        declare p_idSondeur int(3);
        insert into sondeur values(null, p_nom, p_email, p_tel, p_adresse, p_mdp, p_role); 
 
        select idSondeur into p_idSondeur from sondeur 
        where nom = p_nom and email = p_email and tel = p_tel and adresse = p_adresse and mdp = p_mdp and role = p_role; 
 
        insert into personne values (p_idSondeur, p_prenom, p_age);
 
end $

delimiter ;

/* Procedure qui permet d'inserer des sondeurs Entrprise*/
delimiter $
create procedure insertEntreprise (IN p_nom varchar(50), IN p_email varchar(50),
IN p_tel varchar(50), IN p_adresse varchar(50), IN p_mdp varchar(50), IN p_role enum("user","admin"), IN p_libelle varchar(100), IN p_sigle varchar(100), IN p_siret varchar(100))
begin
       declare p_idSondeur int(3);
       insert into sondeur values(null, p_nom, p_email, p_tel, p_adresse, p_mdp, p_role); 

       select idSondeur into p_idSondeur from sondeur 
       where nom = p_nom and email = p_email and tel = p_tel and adresse = p_adresse and mdp = p_mdp and role = p_role; 

       insert into entreprise values (p_idSondeur, p_libelle, p_sigle, p_siret);
end $

delimiter ;


/* Procedure qui permet d'inserer des qestions et reponse */
delimiter $
create procedure insertQuestionReponse(IN p_question varchar(50), IN p_reponse varchar(50))
begin
       declare p_idQuestion int(3);
       insert into question values(null, p_question);

       select idQuestion into p_idQuestion from question 
       where question = p_question;

       insert into reponse values (p_idQuestion, p_reponse); 
end $
delimiter ;



/* Procedure qui permet de recevoir un sondeur et qui réalise la suppression du sondeur dans la table fille personne*/
delimiter  $
create procedure deletePersonne(IN p_idSondeur int(3))
begin
        delete from personne where idSondeur = p_idSondeur;
        delete from sondeur where idSondeur = p_idSondeur;
end  $
delimiter ;


/* Procedure qui permet de recevoir un sondeur et qui réalise la suppression du sondeur dans la table fille entreprise*/
delimiter  $
create procedure deleteEntreprise(IN p_idSondeur int(3))
begin
        delete from entreprise where idSondeur = p_idSondeur;
        delete from sondeur where idSondeur = p_idSondeur;
end  $
delimiter ;


/* Procedure qui permet d'afficher la liste des sous_categorie coresspondant à sa categorie*/
delimiter $
create procedure listeCats( )
begin 
        select so.nom, c.nom as Nom_Categoire
        from sous_categorie so , categorie c
        where so.idCategorie = c.idCategorie 
        group by so.nom;
end $
delimiter ;
