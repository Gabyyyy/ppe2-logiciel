package controleur;

public class Entreprise {
	private int idSondeur;
	private String libelle, sigle, siret;
	
	public Entreprise(int idSondeur, String libelle, String sigle, String siret) 
	{
		this.idSondeur = idSondeur;
		this.libelle = libelle;
		this.sigle = sigle;
		this.siret = siret;
	}
	
	public Entreprise(String libelle, String sigle, String siret) 
	{
		this.idSondeur = 0;
		this.libelle = libelle;
		this.sigle = sigle;
		this.siret = siret;
	}

	public int getIdSondeur() {
		return idSondeur;
	}

	public void setIdSondeur(int idSondeur) {
		this.idSondeur = idSondeur;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public String getSigle() {
		return sigle;
	}

	public void setSigle(String sigle) {
		this.sigle = sigle;
	}

	public String getSiret() {
		return siret;
	}

	public void setSiret(String siret) {
		this.siret = siret;
	}
	
	
	
}
