package controleur;

public class Personne {
	private int idSondeur, age;
	private String prenom;
	
	public Personne(int idSondeur, int age, String prenom) {
		this.idSondeur = idSondeur;
		this.age = age;
		this.prenom = prenom;
	}
	public Personne(int age, String prenom) {
		this.idSondeur = 0;
		this.age = age;
		this.prenom = prenom;
	}
	
	public int getIdSondeur() {
		return idSondeur;
	}
	public void setIdSondeur(int idSondeur) {
		this.idSondeur = idSondeur;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	
}
