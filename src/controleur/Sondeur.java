package controleur;

public class Sondeur {
	private int idSondeur;
	private String nom, email, tel, adresse, mdp, role;
	
	public Sondeur(int idSondeur, String nom, String email, String tel, String adresse, String mdp, String role) {
		this.idSondeur = idSondeur;
		this.nom = nom;
		this.email = email;
		this.tel = tel;
		this.adresse = adresse;
		this.mdp = mdp;
		this.role = role;
	}
	public Sondeur(String nom, String email, String tel, String adresse, String mdp, String role) {
		this.idSondeur = 0;
		this.nom = nom;
		this.email = email;
		this.tel = tel;
		this.adresse = adresse;
		this.mdp = mdp;
		this.role = role;
	}
	public int getIdSondeur() {
		return idSondeur;
	}
	public void setIdSondeur(int idSondeur) {
		this.idSondeur = idSondeur;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public String getMdp() {
		return mdp;
	}
	public void setMdp(String mdp) {
		this.mdp = mdp;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	
	
}
