package controleur;

public class Vue_reponse {
	private int idReponse, idQestion, idSondage, nbReponse;
	private String question, dateCreation, etat, reponse, nom;
	
	public Vue_reponse(int idReponse, int idQestion, int idSondage, int nbReponse, String question, String dateCreation,
			String etat, String reponse, String nom) {
		super();
		this.idReponse = idReponse;
		this.idQestion = idQestion;
		this.idSondage = idSondage;
		this.nbReponse = nbReponse;
		this.question = question;
		this.dateCreation = dateCreation;
		this.etat = etat;
		this.reponse = reponse;
		this.nom = nom;
	}

	public int getIdReponse() {
		return idReponse;
	}

	public void setIdReponse(int idReponse) {
		this.idReponse = idReponse;
	}

	public int getIdQestion() {
		return idQestion;
	}

	public void setIdQestion(int idQestion) {
		this.idQestion = idQestion;
	}

	public int getIdSondage() {
		return idSondage;
	}

	public void setIdSondage(int idSondage) {
		this.idSondage = idSondage;
	}

	public int getNbReponse() {
		return nbReponse;
	}

	public void setNbReponse(int nbReponse) {
		this.nbReponse = nbReponse;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(String dateCreation) {
		this.dateCreation = dateCreation;
	}

	public String getEtat() {
		return etat;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}

	public String getReponse() {
		return reponse;
	}

	public void setReponse(String reponse) {
		this.reponse = reponse;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
}
