package vue;

import java.awt.GridLayout;
import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.JPanel;

import modele.Modele;

public class PanelStats extends PanelDeBase{
	
	private JPanel panelCount = new JPanel();
	public PanelStats() {
	
		super (Color.cyan);
		//construction d'un panel Count
		this.panelCount.setBounds(100, 100, 500, 200);
		this.panelCount.setLayout(new GridLayout(4, 1));
		int nbCategories = Modele.countCategorie();
		int nbSousCategories = Modele.countSous_categorie();
		int nbSondages = Modele.countSondage();
		int total = nbCategories+nbSousCategories+nbSondages;
		
		this.panelCount.add(new JLabel("Nombre de Categories    : " + nbCategories));
		this.panelCount.add(new JLabel("Nombre de Sous-Categories : " + nbSousCategories)); 
		this.panelCount.add(new JLabel("Nombre de Sondages  : " + nbSondages));
		this.panelCount.add(new JLabel("Total  : " + total));
		this.panelCount.setVisible(true);
		this.add(panelCount);
	}
	
	public void Actualiser()
	{
		this.panelCount.removeAll();
		
		int nbCategories = Modele.countCategorie();
		int nbSousCategories = Modele.countSous_categorie();
		int nbSondages = Modele.countSondage();
		int total = nbCategories+nbSousCategories+nbSondages;
		
		this.panelCount.add(new JLabel("Nombre de Categories    : " + nbCategories));
		this.panelCount.add(new JLabel("Nombre de Sous-Categories : " + nbSousCategories)); 
		this.panelCount.add(new JLabel("Nombre de Sondages  : " + nbSondages));
		this.panelCount.add(new JLabel("Total  : " + total));
		this.panelCount.setVisible(true);
	}

}
